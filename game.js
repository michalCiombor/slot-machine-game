class Game{
    constructor(start){
this.stats = new Statistics();
this.wallet = new Wallet(start);
document.getElementById("start").addEventListener("click", this.startGame.bind(this));
this.spanWallet = document.querySelector(".result");
this.boards = document.querySelectorAll(".kwadrat");
this.inputBid = document.getElementById("bid");
this.spanResult = document.querySelector("span[data-set='przegrales'");
this.spanGames = document.querySelector("span[data-set='liczbaGier']");
this.spanWins = document.querySelector("span[data-set='wygranych']");
this.spanLosses = document.querySelector("span[data-set='przegranych']");

this.render();

    }
    render(colors=['gray','gray','gray'], money = this.wallet.getWalletValue(),result="", stats = [0,0,0], bid=0, wonMoney=0){
        // console.log("gramy!!")
        this.spanWallet.textContent=money;
       
        if(result){
            
            result=`wygrałeś ${wonMoney} $`;
        }else if (!result && result !==""){
        result = `Przegrałeś ${bid} $`
    }
        this.spanResult.textContent=result;
        this.spanGames.textContent=stats[0];
        this.spanWins.textContent=stats[1];
        this.spanLosses.textContent=stats[2];
        this.inputBid.value="";
        this.boards.forEach((board,index)=>{
            board.style.backgroundColor= colors[index]
        });
        

    }
    startGame(){
if(this.inputBid.value <1)return alert("kwota, którą chcesz grać jest za mała");
const bid = Math.floor(this.inputBid.value);
if (!this.wallet.checkCanPlay(bid)){
return alert("masz za mało środków lub podana wartość jest nieprawidłowa")
}    

this.wallet.changeWallet(bid, '-');
this.draw = new Draw();
const colors=this.draw.getDrawResult();
const win = Result.checkWinner(colors);
// console.log(win);
// console.log(colors);
const wonMoney = Result.moneyWinInGame(win, bid);
// console.log(wonMoney);
this.wallet.changeWallet(wonMoney);
this.stats.addGameToStatistics(win, bid);

this.render(colors, this.wallet.getWalletValue(), win, this.stats.showGameStatistics(), bid, wonMoney);
}
}

