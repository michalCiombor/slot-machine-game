class Draw {
    constructor() {
        this.options = ['#fa8072', '#98fb98', '#e6e6fa'];
        let _result = this.drawResult();
        this.getDrawResult = () => _result;
    }

    drawResult() {
        let colors = [];
        //uzupełnianie poprzez losowanie
        for (let i = 0; i < this.options.length; i++) {
            const index = Math.floor(Math.random() * this.options.length)

            const color = this.options[index];
            colors.push(color);
            // console.log(color);
        }
        return colors;
    }

}
const draw = new Draw();